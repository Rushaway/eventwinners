#pragma semicolon 1
#include <sdktools>
#include <cstrike>
#include <zombiereloaded>
#include <multicolors>

#define PLUGIN_VERSION "2.0"

public Plugin myinfo = 
{
    name = "Event Winners Log",
    author = "Nano, maxime1907",
    description = "Command to print a log list with every alive human from the last round played",
    version = PLUGIN_VERSION,
    url = "http://steamcommunity.com/id/marianzet1"
};

char g_sMap[128], g_sPath[PLATFORM_MAX_PATH];
int g_iSurvivors[MAXPLAYERS+1];

public void OnPluginStart()
{
    HookEvent("round_end", EventRoundEnd, EventHookMode_Pre);

    RegAdminCmd("sm_winners", Command_winners, ADMFLAG_GENERIC);	

    // char sDate[12];
    // FormatTime(sDate, sizeof(sDate), "%y-%m-%d");
    // BuildPath(Path_SM, g_sPath, sizeof(g_sPath), "logs/events/winners_%s.log", sDate);	
}

public void OnMapStart() 
{
    for(int i = 0; i < sizeof g_iSurvivors; i++) g_iSurvivors[i] = 0;

    // GetCurrentMap(g_sMap, sizeof g_sMap);
    // GetMapDisplayName(g_sMap, g_sMap, sizeof g_sMap);

    // char szMap[64]; GetCurrentMap(szMap, sizeof(szMap));
    // LogToFile(g_sPath, "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Map has changed to %s ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~", szMap);	
}

public void EventRoundEnd(Handle hEvent, char[] chEvent, bool bDontBroadcast)
{
    for(int i = 1; i <= MaxClients; i++)
    {
        if(IsClientInGame(i) && IsPlayerAlive(i) && GetClientTeam(i) == CS_TEAM_CT)
        {
            g_iSurvivors[i] = GetClientUserId(i);
        }
        else g_iSurvivors[i] = 0;
    }
}

public Action Command_winners(int iClient, int arg)
{
    if(iClient < 1 || !IsClientInGame(iClient)) return Plugin_Handled;

    // LogToFile(g_sPath, "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Map: %s | Round %d | Log printed by %N ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~", g_sMap, GetTeamScore(2)+GetTeamScore(3), iClient);
    PrintToConsole(iClient, "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Map: %s | Round %d | Log printed by %N ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~", g_sMap, GetTeamScore(CS_TEAM_CT), iClient);

    char sPlayerAuth[24]; 
    for(int i = 1; i <= MaxClients; i++)
    {
        if(IsClientInGame(i) && g_iSurvivors[i] == GetClientUserId(i))
        {
            GetClientAuthId(i, AuthId_Steam2, sPlayerAuth, sizeof(sPlayerAuth));
            // LogToFile(g_sPath, "%40N %24s", i, sPlayerAuth);
            PrintToConsole(iClient, "%40N %24s", i, sPlayerAuth);
        }
    }

    CPrintToChatAll("{green}[Winners]{default} Winners list has been logged");

    return Plugin_Handled;
}
